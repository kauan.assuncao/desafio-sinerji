### Pré requisitos
Para rodar o projeto é necessário ter o [docker](https://www.docker.com/get-started/) instalado

### Instalação

Clonar o projeto
```shell
    git clone git@gitlab.com:kauan.assuncao/desafio-sinerji.git
```

Acesse a pasta do projeto
```shell
    cd caminho/do/projeto
```

Monte a imagem do servidor
```shell
    docker build -t desafio/projeto .
```

Inicialize o container
```shell
    docker run -it --rm --name servidor -p 8080:8080 -p 9990:9990 desafio/projeto
```