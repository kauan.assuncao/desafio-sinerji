package br.com.desafio.model;

import java.util.UUID;

public class PersonResponseStub {
	
	private PersonResponse response;

	private PersonResponseStub() {
		response = new PersonResponse();
	}
	
	public static PersonResponseStub newInstance() {
		return new PersonResponseStub();
	}
	
	public PersonResponseStub withId(UUID id) {
		response.setId(id.toString());
		return this;
	}
	
	public PersonResponse build() {
		return response;
	}

}
