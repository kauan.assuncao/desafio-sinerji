package br.com.desafio.model;

import java.time.LocalDate;
import java.util.UUID;

public class PersonFormStub {
	
	private PersonForm form;

	private PersonFormStub() {
		form = new PersonForm();
	}
	
	public static PersonFormStub newInstance() {
		return new PersonFormStub();
	}
	
	public PersonFormStub withId(UUID id) {
		form.setId(id.toString());
		return this;
	}
	
	public PersonFormStub withName(String name) {
		form.setName(name);
		return this;
	}
	
	public PersonFormStub withBirth(LocalDate birth) {
		form.setBirth(birth);
		return this;
	}
	
	public PersonFormStub withGender(Gender gender) {
		form.setGender(gender);
		return this;
	}
	
	public PersonFormStub withState(String state) {
		form.setState(state);
		return this;
	}
	
	public PersonFormStub withCity(String city) {
		form.setCity(city);
		return this;
	}
	
	public PersonFormStub withAddress(String address) {
		form.setAddress(address);
		return this;
	}
	
	public PersonFormStub withNumber(int number) {
		form.setNumber(number);
		return this;
	}
	
	public PersonFormStub withZipcode(String zipcode) {
		form.setZipcode(zipcode);
		return this;
	}

	public PersonForm build() {
		return form;
	}
}
