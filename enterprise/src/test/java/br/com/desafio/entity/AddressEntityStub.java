package br.com.desafio.entity;

public class AddressEntityStub {
	
	private AddressEntity address;
	
	private AddressEntityStub() {
		address = new AddressEntity();
	}
	
	public static AddressEntityStub newInstance() {
		return new AddressEntityStub();
	}
	
	public AddressEntityStub withState(String state) {
		address.setState(state);
		return this;
	}
	
	public AddressEntityStub withCity(String city) {
		address.setCity(city);
		return this;
	}
	
	public AddressEntityStub withAddress(String address) {
		this.address.setAddress(address);
		return this;
	}
	
	public AddressEntityStub withNumber(int number) {
		address.setNumber(number);
		return this;
	}
	
	public AddressEntityStub withZipcode(String zipcode) {
		this.address.setZipcode(zipcode);
		return this;
	}

	public AddressEntity build() {
		return address;
	}

}
