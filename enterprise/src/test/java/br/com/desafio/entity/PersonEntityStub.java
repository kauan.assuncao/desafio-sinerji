package br.com.desafio.entity;

import java.time.LocalDate;
import java.util.UUID;
import java.util.function.UnaryOperator;

import br.com.desafio.model.Gender;

public class PersonEntityStub {
	
	private PersonEntity person;
	
	private PersonEntityStub() {
		person = new PersonEntity();
	}
	
	public static PersonEntityStub newInstance() {
		return new PersonEntityStub();
	}

	public PersonEntityStub withId(UUID id) {
		person.setId(id.toString());
		return this;
	}
	
	public PersonEntityStub withName(String name) {
		person.setName(name);
		return this;
	}
	
	public PersonEntityStub withBirth(LocalDate birth) {
		person.setBirth(birth);
		return this;
	}
	
	public PersonEntityStub withGender(Gender gender) {
		person.setGender(gender.name());
		return this;
	}
	
	public PersonEntityStub withAddress(UnaryOperator<AddressEntityStub> builder) {
		AddressEntity address = builder.apply(AddressEntityStub.newInstance()).build();
		person.setAddress(address);
		return this;
	}

	public PersonEntity build() {
		return person;
	}

}
