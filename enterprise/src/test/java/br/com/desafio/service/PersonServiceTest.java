package br.com.desafio.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.desafio.entity.AddressEntity;
import br.com.desafio.entity.PersonEntity;
import br.com.desafio.entity.PersonEntityStub;
import br.com.desafio.model.Gender;
import br.com.desafio.model.PersonForm;
import br.com.desafio.model.PersonFormStub;
import br.com.desafio.model.PersonResponse;
import br.com.desafio.model.PersonResponseStub;
import br.com.desafio.repository.PersonRepository;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

	@Mock
	private PersonRepository repository;
	
	@Captor
	private ArgumentCaptor<PersonEntity> personCaptor;
	
	@Captor
	private ArgumentCaptor<List<String>> personIdsCaptor;

	@InjectMocks
	private PersonService service;
			
	public PersonEntityStub personStub;
	
	@BeforeEach
	public void setup() {
		personStub = PersonEntityStub.newInstance()
				.withId(UUID.randomUUID())
				.withName("Teste")
				.withGender(Gender.M)
				.withBirth(LocalDate.now())
				.withAddress(address -> address
						.withAddress("Teste QD")
						.withCity("Cidade")
						.withState("Estado")
						.withNumber(12)
						.withZipcode("789456123"));
	}

	@Test
	void shouldFindAllPersons() {
		PersonEntity person = personStub.build();
		
		doReturn(List.of(person))
			.when(repository)
			.find();
		
		List<PersonResponse> responses = service.findAll();
		
		verify(repository).find();
		assertEquals(1, responses.size());
		PersonResponse response = responses.get(0);
		assertEquals(person.getId(), response.getId());
		assertEquals(person.getName(), response.getName());
		assertEquals(person.getGender(), response.getGender().name());
		
		AddressEntity address = person.getAddress();
		assertEquals(address.getCity(), response.getCity());
		assertEquals(address.getState(), response.getState());
	}
	
	@Test
	void shouldFindPersonById() {
		final PersonEntity person = personStub.withId(UUID.randomUUID()).build();

		doReturn(person)
			.when(repository)
			.findById(person.getId());
		
		PersonForm form = service.findById(person.getId());
		
		verify(repository).findById(person.getId());
		assertEquals(person.getId(), form.getId());
		assertEquals(person.getName(), form.getName());
		assertEquals(person.getBirth(), form.getBirth());
		assertEquals(person.getGender(), form.getGender().name());
		assertEquals(person.getAddress().getAddress(), form.getAddress());
		assertEquals(person.getAddress().getCity(), form.getCity());
		assertEquals(person.getAddress().getNumber(), form.getNumber());
		assertEquals(person.getAddress().getState(), form.getState());
		assertEquals(person.getAddress().getZipcode(), form.getZipcode());
	}

	@Test
	void shoudlCreatePerson() {
		PersonForm form = PersonFormStub.newInstance()
			.withId(UUID.randomUUID())
			.withName("Test")
			.withBirth(LocalDate.now())
			.withGender(Gender.F)
			.withAddress("QD 01 Lote 11")
			.withCity("Brasília")
			.withNumber(1)
			.withState("DF")
			.withZipcode("71995000")
			.build();
		
		PersonResponse response = service.create(form);
		
		verify(repository).save(personCaptor.capture());
		
		PersonEntity entity = personCaptor.getValue();
		assertEquals(form.getId(), entity.getId());
		assertEquals(form.getName(), entity.getName());
		assertEquals(form.getBirth(), entity.getBirth());
		assertEquals(form.getGender().name(), entity.getGender());
		assertEquals(form.getAddress(), entity.getAddress().getAddress());
		assertEquals(form.getCity(), entity.getAddress().getCity());
		assertEquals(form.getNumber(), entity.getAddress().getNumber());
		assertEquals(form.getState(), entity.getAddress().getState());
		assertEquals(form.getZipcode(), entity.getAddress().getZipcode());

		assertEquals(entity.getId(), response.getId());
		assertEquals(entity.getName(), response.getName());
		assertEquals(entity.getGender(), response.getGender().name());

		AddressEntity address = entity.getAddress();
		assertEquals(address.getCity(), response.getCity());
		assertEquals(address.getState(), response.getState());
	}

	@Test
	public void shouldUpdatePerson() {
		PersonForm form = PersonFormStub.newInstance()
				.withId(UUID.randomUUID())
				.withName("Test")
				.withBirth(LocalDate.now())
				.withGender(Gender.F)
				.withAddress("QD 01 Lote 11")
				.withCity("Brasília")
				.withNumber(1)
				.withState("DF")
				.withZipcode("71995000")
				.build();
			
			PersonResponse response = service.update(form);
			
			verify(repository).update(personCaptor.capture());
			
			PersonEntity entity = personCaptor.getValue();
			assertEquals(form.getId(), entity.getId());
			assertEquals(form.getName(), entity.getName());
			assertEquals(form.getBirth(), entity.getBirth());
			assertEquals(form.getGender().name(), entity.getGender());
			assertEquals(form.getAddress(), entity.getAddress().getAddress());
			assertEquals(form.getCity(), entity.getAddress().getCity());
			assertEquals(form.getNumber(), entity.getAddress().getNumber());
			assertEquals(form.getState(), entity.getAddress().getState());
			assertEquals(form.getZipcode(), entity.getAddress().getZipcode());

			assertEquals(entity.getId(), response.getId());
			assertEquals(entity.getName(), response.getName());
			assertEquals(entity.getGender(), response.getGender().name());

			AddressEntity address = entity.getAddress();
			assertEquals(address.getCity(), response.getCity());
			assertEquals(address.getState(), response.getState());
	}

	@Test
	public void shouldRemovePersonById() {
		final String personId = UUID.randomUUID().toString();
		
		service.remove(personId);
		
		verify(repository).remove(personId);
	}

	@Test
	public void shouldRemoveManyPersonsById() {
		final List<PersonResponse> persons = List.of(
				PersonResponseStub.newInstance()
					.withId(UUID.randomUUID())
					.build(),
				PersonResponseStub.newInstance()
					.withId(UUID.randomUUID())
					.build());

		service.removeMany(persons);
		
		verify(repository).removeMany(personIdsCaptor.capture());
		
		List<String> personIds = personIdsCaptor.getValue();
		assertEquals(2, personIds.size());
		assertEquals(persons.get(0).getId(), personIds.get(0));
		assertEquals(persons.get(1).getId(), personIds.get(1));
	}
}
