package br.com.desafio.transform;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import br.com.desafio.entity.AddressEntity;
import br.com.desafio.entity.PersonEntity;
import br.com.desafio.model.Gender;
import br.com.desafio.model.PersonForm;
import br.com.desafio.model.PersonResponse;

class PersonTransformTest {
	
	LocalDate date = LocalDate.now();

	@Test
	void shouldTransformToPersonResponse() {
		PersonEntity person = new PersonEntity();
		person.setId(UUID.randomUUID().toString());
		person.setName("Fulano");
		person.setBirth(date);
		person.setGender("M");
		
		AddressEntity address = new AddressEntity();
		address.setAddress("QD 01 Lote 11");
		address.setCity("Brasília");
		address.setState("DF");
		person.setAddress(address);
		
		PersonResponse response = PersonTransform.toPersonResponse(person);

		assertEquals(person.getId(), response.getId());
		assertEquals(person.getName(), response.getName());
		assertEquals(person.getGender(), response.getGender().name());
		assertEquals(address.getCity(), response.getCity());
		assertEquals(address.getState(), response.getState());
	}
	
	@Test
	void shouldTransformToPersonEntity() {
		PersonForm personForm = new PersonForm();
		personForm.setId(UUID.randomUUID().toString());
		personForm.setName("Test");
		personForm.setBirth(date.plusDays(1));
		personForm.setGender(Gender.F);
		personForm.setAddress("QD 01 Lote 11");
		personForm.setCity("Brasília");
		personForm.setNumber(1);
		personForm.setState("DF");
		personForm.setZipcode("71995000");

		PersonEntity responseEntity = PersonTransform.toPersonEntity(personForm);
		
		assertEquals(personForm.getId(), responseEntity.getId());
		assertEquals(personForm.getName(), responseEntity.getName());
		assertEquals(personForm.getBirth(), responseEntity.getBirth());
		assertEquals(personForm.getGender().name(), responseEntity.getGender());
		assertEquals(personForm.getAddress(), responseEntity.getAddress().getAddress());
		assertEquals(personForm.getCity(), responseEntity.getAddress().getCity());
		assertEquals(personForm.getNumber(), responseEntity.getAddress().getNumber());
		assertEquals(personForm.getState(), responseEntity.getAddress().getState());
		assertEquals(personForm.getZipcode(), responseEntity.getAddress().getZipcode());

	}
	
	@Test
	void shouldTransformToPersonForm() {
		PersonEntity person = new PersonEntity();
		person.setId(UUID.randomUUID().toString());
		person.setName("Fulano");
		person.setBirth(date);
		person.setGender("M");
		
		AddressEntity address = new AddressEntity();
		address.setAddress("QD 01 Lote 11");
		address.setCity("Brasília");
		address.setState("DF");
		address.setNumber(1);
		address.setZipcode("71995000");
		person.setAddress(address);
		

		PersonForm form = PersonTransform.toPersonForm(person);
		
		assertEquals(person.getId(), form.getId());
		assertEquals(person.getName(), form.getName());
		assertEquals(person.getBirth(), form.getBirth());
		assertEquals(person.getGender(), form.getGender().name());
		assertEquals(person.getAddress().getAddress(), form.getAddress());
		assertEquals(person.getAddress().getCity(), form.getCity());
		assertEquals(person.getAddress().getNumber(), form.getNumber());
		assertEquals(person.getAddress().getState(), form.getState());
		assertEquals(person.getAddress().getZipcode(), form.getZipcode());

	}

}
