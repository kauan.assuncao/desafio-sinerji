package br.com.desafio.model;

public enum Gender {
	F("Feminino"), M("Masculino");

	String gender;

	Gender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String getName() {
		return name();
	}
}
