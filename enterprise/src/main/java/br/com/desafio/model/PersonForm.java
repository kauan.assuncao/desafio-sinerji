package br.com.desafio.model;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonForm implements Serializable {
		private static final long serialVersionUID = 1L;
		private String id;
		private String name;
		private LocalDate birth;
		private Gender gender;
		private String state;
		private String city;
		private String address;
		private Integer number;
		private String zipcode;
		
		
		public PersonForm() {
			gender = Gender.F;
		}
		
		public PersonForm(String id) {
			this();
			this.id = id;
		}
}
