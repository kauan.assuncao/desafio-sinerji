package br.com.desafio.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.PrimeFaces;

import br.com.desafio.model.Gender;
import br.com.desafio.model.PersonForm;
import br.com.desafio.model.PersonResponse;
import br.com.desafio.service.PersonService;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
@ViewScoped
public class PersonView implements Serializable {
	private static final long serialVersionUID = 1L;

	private PersonResponse personResponse;
	private PersonForm personForm;
	private List<PersonResponse> persons;
	private List<PersonResponse> selectedPersons;

	@Inject
	private PersonService service;

	@PostConstruct
	public void init() {
		persons = new ArrayList<>(service.findAll());
		selectedPersons = new ArrayList<>();
	}

	public void openNew() {
		personForm = new PersonForm();
	}
	
	public void editPerson() {
		personForm = service.findById(personResponse.getId());
	}

	public void savePerson() {
		if(personForm.getId() == null) {
			PersonResponse response = service.create(personForm);
			persons.add(response);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Pessoa Cadastrada"));
		} else {
			PersonResponse response = service.update(personForm);
			int index = 0;
			for(; index < persons.size(); index++) {
				if(persons.get(index).getId() == personForm.getId()) break;
			}
			persons.set(index, response);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Pessoa Atualizada"));
		}

		PrimeFaces.current().executeScript("PF('managePersonDialog').hide()");
		PrimeFaces.current().ajax().update("form:messages", "form:dt-persons");
	}

	public void removePerson() {
		service.remove(personResponse.getId());
		persons.remove(personResponse);
		selectedPersons.remove(personResponse);
		
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Pessoa Removida"));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-persons");
	}

    public String getDeleteButtonMessage() {
        if (hasSelectedPersons()) {
            int size = this.selectedPersons.size();
            return size > 1 ? size + " pessoas selecionadas" : "1 pessoa selecionada";
        }

        return "Remover";
    }

    public boolean hasSelectedPersons() {
        return this.selectedPersons != null && !this.selectedPersons.isEmpty();
    }

    public void deleteSelectedPersons() {
        service.removeMany(selectedPersons);
        persons.removeAll(selectedPersons);
        selectedPersons = new ArrayList<>();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Pessoa Removida"));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-persons");
        PrimeFaces.current().executeScript("PF('dtPersons').clearFilters()");
    }
	
	public List<PersonResponse> getPersons() {
		return persons;
	}
	
	public List<PersonResponse> getSelectedPersons() {
		return selectedPersons;
	}
	
	public void setSelectedPersons(List<PersonResponse> selectedPersons) {
		this.selectedPersons = selectedPersons;
	}
	
	public PersonResponse getPersonResponse() {
		return personResponse;
	}
	
	public void setPersonResponse(PersonResponse personResponse) {
		this.personResponse = personResponse;
	}
	
	public PersonForm getPersonForm() {
		return personForm;
	}

	public void setPersonForm(PersonForm personForm) {
		this.personForm = personForm;
	}
	
	public Gender[] getGenders() {
		return Gender.values();
	}
}
