package br.com.desafio.entity;

import jakarta.persistence.Table;

@Table(name = "address")
@jakarta.persistence.Entity
public class AddressEntity extends Entity {

	private String state;
	private String city;
	private String address;
	private Integer number;
	private String zipcode;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
