package br.com.desafio.entity;

import java.time.LocalDate;

import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Table(name = "person")
@jakarta.persistence.Entity
public class PersonEntity extends Entity {

	private String name;
	private LocalDate birth;
	private String gender;
	
	@ManyToOne()
	private AddressEntity address;
}
