package br.com.desafio.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

public abstract class Repository {
	@PersistenceContext(unitName = "enterprisePU") EntityManager manager;
}
