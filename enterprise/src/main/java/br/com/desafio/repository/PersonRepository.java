package br.com.desafio.repository;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import br.com.desafio.entity.PersonEntity;
import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class PersonRepository extends Repository implements Serializable {
	private static final long serialVersionUID = 1L;

	public List<PersonEntity> find() {
		return manager.createQuery("select person FROM PersonEntity as person", PersonEntity.class).getResultList();
	}

	public PersonEntity findById(String personId) {
		return manager.find(PersonEntity.class, personId);
	}

	public void save(PersonEntity person) {
		person.setId(UUID.randomUUID().toString());
		manager.merge(person);
	}

	public void update(PersonEntity person) {
		manager.persist(person);
	}

	public void remove(String personId) {
		PersonEntity entity = findById(personId);
		manager.remove(entity);

	}

	public void removeMany(List<String> personIds) {
		String query = "delete from PersonEntity as person where person.id in :ids";

		manager.createQuery(query).setParameter("ids", personIds).executeUpdate();

	}
}
