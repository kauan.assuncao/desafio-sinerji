package br.com.desafio.transform;

import br.com.desafio.entity.AddressEntity;
import br.com.desafio.entity.PersonEntity;
import br.com.desafio.model.Gender;
import br.com.desafio.model.PersonForm;
import br.com.desafio.model.PersonResponse;

public abstract class PersonTransform {

	public static PersonResponse toPersonResponse(PersonEntity person) {
		PersonResponse response = new PersonResponse();
		response.setId(person.getId());
		response.setName(person.getName());
		response.setBirth(person.getBirth());
		response.setGender(Gender.valueOf(person.getGender()));
		
		AddressEntity address = person.getAddress();
		response.setCity(address.getCity());
		response.setState(address.getState());
		
		return response;
	}
	
	public static PersonEntity toPersonEntity(PersonForm personForm) {
		PersonEntity person = new PersonEntity();
		person.setId(personForm.getId());
		person.setName(personForm.getName());
		person.setBirth(personForm.getBirth());
		person.setGender(personForm.getGender().name());
	
		AddressEntity address = new AddressEntity();
		address.setAddress(personForm.getAddress());
		address.setCity(personForm.getCity());
		address.setNumber(personForm.getNumber());
		address.setState(personForm.getState());
		address.setZipcode(personForm.getZipcode());
		person.setAddress(address);
		
		return person;
	}

	public static PersonForm toPersonForm(PersonEntity person) {
		PersonForm form = new PersonForm(person.getId());
		form.setName(person.getName());
		form.setGender(Gender.valueOf(person.getGender()));
		form.setBirth(person.getBirth());
		
		AddressEntity address = person.getAddress();
		form.setCity(address.getCity());
		form.setAddress(address.getAddress());
		form.setNumber(address.getNumber());
		form.setState(address.getState());
		form.setZipcode(address.getZipcode());
		
		return form;
	}
}
