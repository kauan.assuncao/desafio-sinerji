package br.com.desafio.service;

import java.io.Serializable;
import java.util.List;

import br.com.desafio.entity.PersonEntity;
import br.com.desafio.model.PersonForm;
import br.com.desafio.model.PersonResponse;
import br.com.desafio.repository.PersonRepository;
import br.com.desafio.transform.PersonTransform;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Stateless
public class PersonService implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PersonRepository repository;
	
	public List<PersonResponse> findAll() {
		log.info("Finding all persons");
		return repository.find()
			.stream()
			.map(PersonTransform::toPersonResponse)
			.toList();
	}

	public PersonForm findById(String personId) {
		log.info("Finding person by id {}", personId);
		PersonEntity person = repository.findById(personId);
		return PersonTransform.toPersonForm(person);
	}
	
	public PersonResponse create(PersonForm personForm) {
		log.info("Creating person {}", personForm);
		PersonEntity person = PersonTransform.toPersonEntity(personForm);
		repository.save(person);
		
		return PersonTransform.toPersonResponse(person);
	}

	public PersonResponse update(PersonForm personForm) {
		log.info("Updating person {}", personForm);
		PersonEntity person = PersonTransform.toPersonEntity(personForm);
		repository.update(person);
		
		return PersonTransform.toPersonResponse(person);
	}

	public void remove(String personId) {
		log.info("Remove person with id {}", personId);
		repository.remove(personId);
	}

	public void removeMany(List<PersonResponse> persons) {
		log.info("Remove persons on list id {}", persons);
		List<String> ids = persons.stream().map(PersonResponse::getId).toList();
		repository.removeMany(ids);
	}
}
